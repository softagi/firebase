package softagi.ss.firebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity
{
    private EditText emailField, passwordField;
    private FirebaseAuth auth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        auth = FirebaseAuth.getInstance();
        initViews();
        initDialog();
    }

    private void initDialog()
    {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("login ...");
        progressDialog.setMessage("please wait ...");
        progressDialog.setCancelable(false);
    }

    private void initViews()
    {
        emailField = findViewById(R.id.email_field);
        passwordField = findViewById(R.id.password_field);
    }

    public void register(View view)
    {
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
    }

    public void login(View view)
    {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();

        if (email.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter email", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.show();
        loginFirebase(email, password);
    }

    private void loginFirebase(String email, String password)
    {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                progressDialog.dismiss();

                if (task.isSuccessful())
                {
                    if (task.getResult().getUser().isEmailVerified())
                    {
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        finish();
                    } else
                        {
                            Toast.makeText(getApplicationContext(), "please check your email", Toast.LENGTH_SHORT).show();
                        }
                } else
                    {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
            }
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null)
        {
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            finish();
        }
    }
}