package softagi.ss.firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import softagi.ss.firebase.models.userModel;

public class RegisterActivity extends AppCompatActivity
{
    private EditText usernameField,emailField, passwordField, confirmPasswordField, mobileField, addressField;
    private CircleImageView circleImageView;
    private Uri imageUri;
    private ProgressDialog progressDialog;

    // firebase auth
    private FirebaseAuth auth;

    // firebase database
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    // firebase storage
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initFirebase();
        initViews();
        initDialog();
        chooseImage();
    }

    private void initFirebase()
    {
        auth = FirebaseAuth.getInstance();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        firebaseStorage = FirebaseStorage.getInstance();
    }

    private void chooseImage()
    {
        circleImageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 505);
            }
        });
    }

    private void initDialog()
    {
        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setTitle("register ...");
        progressDialog.setMessage("please wait ...");
        progressDialog.setCancelable(false);
    }

    private void initViews()
    {
        usernameField = findViewById(R.id.username_field);
        emailField = findViewById(R.id.email_field);
        passwordField = findViewById(R.id.password_field);
        confirmPasswordField = findViewById(R.id.confirm_password_field);
        mobileField = findViewById(R.id.mobile_field);
        addressField = findViewById(R.id.address_field);
        circleImageView = findViewById(R.id.user_image);
    }

    public void register(View view)
    {
        String username = usernameField.getText().toString();
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();
        String confirm = confirmPasswordField.getText().toString();
        String mobile = mobileField.getText().toString();
        String address = addressField.getText().toString();

        if (imageUri == null)
        {
            Toast.makeText(getApplicationContext(), "select an image", Toast.LENGTH_SHORT).show();
            return;
        }

        if (username.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter username", Toast.LENGTH_SHORT).show();
            return;
        }

        if (email.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter email", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!confirm.equals(password))
        {
            Toast.makeText(getApplicationContext(), "password doesn't match", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mobile.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter mobile", Toast.LENGTH_SHORT).show();
            return;
        }

        if (address.isEmpty())
        {
            Toast.makeText(getApplicationContext(), "enter address", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.show();
        registerFirebase(username, email, password, mobile, address, imageUri);
    }

    private void registerFirebase(final String username, final String email, String password, final String mobile, final String address, final Uri imageUri)
    {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    /// from mans
                    // from alaa
                    task.getResult().getUser().sendEmailVerification();
                    String uId = task.getResult().getUser().getUid();

                    uploadData(username, email, mobile, address, imageUri, uId);
                } else
                {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void uploadData(final String username, final String email, final String mobile, final String address, Uri imageUri, final String uId)
    {
        storageReference = firebaseStorage.getReference().child("users_images/" + imageUri.getLastPathSegment());
        UploadTask uploadTask = storageReference.putFile(imageUri);

        Task<Uri> task = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>()
        {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
            {
                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>()
        {
            @Override
            public void onComplete(@NonNull Task<Uri> task)
            {
                Uri imageUri = task.getResult();
                String imageUrl = imageUri.toString();

                uploadDatabase(username,email,mobile,address,imageUrl,uId);
            }
        });
    }

    private void uploadDatabase(String username, String email, String mobile, String address, String imageUrl, String uId)
    {
        userModel user = new userModel(username, email, mobile, address, imageUrl);

        databaseReference.child("users").child(uId).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                progressDialog.dismiss();
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 505 && resultCode == RESULT_OK && data != null)
        {
            imageUri = data.getData();
            Picasso.get()
                    .load(imageUri)
                    .into(circleImageView);
        }
    }
}