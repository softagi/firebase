package softagi.ss.firebase;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class EmailVerificationActivity extends AppCompatActivity
{
    private Button home_btn;
    private FirebaseUser user;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);

        user = FirebaseAuth.getInstance().getCurrentUser();
        initViews();
        initDialog();
    }

    private void initDialog()
    {
        progressDialog = new ProgressDialog(EmailVerificationActivity.this);
        progressDialog.setMessage("please wait ...");
    }

    private void initViews()
    {
        home_btn = findViewById(R.id.home_btn);

        home_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                progressDialog.show();

                if (user.isEmailVerified())
                {
                    progressDialog.dismiss();

                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                } else
                    {
                        progressDialog.dismiss();

                        Toast.makeText(getApplicationContext(), "please check your email", Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }
}
