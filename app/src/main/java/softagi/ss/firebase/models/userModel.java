package softagi.ss.firebase.models;

public class userModel
{
    private String username,email,mobile,address,image_url;

    public userModel(String username, String email, String mobile, String address, String image_url) {
        this.username = username;
        this.email = email;
        this.mobile = mobile;
        this.address = address;
        this.image_url = image_url;
    }

    public userModel() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}